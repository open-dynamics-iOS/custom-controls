# ODControls

[![CI Status](http://img.shields.io/travis/farooq82/ODControls.svg?style=flat)](https://travis-ci.org/farooq82/ODControls)
[![Version](https://img.shields.io/cocoapods/v/ODControls.svg?style=flat)](http://cocoapods.org/pods/ODControls)
[![License](https://img.shields.io/cocoapods/l/ODControls.svg?style=flat)](http://cocoapods.org/pods/ODControls)
[![Platform](https://img.shields.io/cocoapods/p/ODControls.svg?style=flat)](http://cocoapods.org/pods/ODControls)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ODControls is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "ODControls"
```

## Author

farooq82, farooq.zaman@me.com

## License

ODControls is available under the MIT license. See the LICENSE file for more info.
