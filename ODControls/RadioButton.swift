//
//  RadioButton.swift
//  Sipoc
//
//  Created by Farooq Zaman on 21/03/2017.
//  Copyright © 2017 Pizza Mania. All rights reserved.
//

import UIKit

@IBDesignable
open class RadioButton: UIButton {

    @IBOutlet public var group: [RadioButton] = []

    @IBInspectable public var isChecked:Bool = false{
        didSet{
            if isChecked{
                group.forEach(){
                    $0.isChecked = false
                }
                self.setImage(imageOn, for: .normal)
            }else{
                self.setImage(imageOff, for: .normal)
            }
        }
    }
    
    @IBInspectable public var imageOn:UIImage? = UIImage(named: "option_on")
    @IBInspectable public var imageOff:UIImage? = UIImage(named: "option_off")
    
    override open func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        if isChecked{
            self.setImage(imageOn, for: .normal)
        }else{
            self.setImage(imageOff, for: .normal)
        }
    }
    
    override open func draw(_ rect: CGRect) {
        
    }
}
