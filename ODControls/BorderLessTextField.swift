//
//  BorderlessTextField.swift
//  Eco Smart
//
//  Created by Ibrahim Bin Farooq on 24/09/2016.
//  Copyright © 2016 scouffe Pvt Ltd. All rights reserved.
//

import UIKit

@IBDesignable
open class BorderLessTextField:UITextField{
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBInspectable
    public var borderWidth:CGFloat = 1.0
    
    @IBInspectable
    public var borderColor:UIColor? = UIColor.secondaryText
    
    @IBInspectable
    public var placeholderColor:UIColor? = UIColor.secondaryText
    
    @IBInspectable
    public var verticalAlignment:Int{
        get{
            return self.contentVerticalAlignment.rawValue
        }
        set{
            self.contentVerticalAlignment = UIControlContentVerticalAlignment(rawValue: newValue) ?? UIControlContentVerticalAlignment.center
        }
    }
    
    @IBInspectable
    public var rightViewImage:UIImage?{
        didSet{
            if let image = rightViewImage{
                self.rightViewMode = .always
                self.rightView = UIImageView(image: image)
            }else{
                self.rightViewMode = .never
                self.rightView = nil
            }
        }
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        addBottomBorder()
        
        if let ph = placeholder, ph.isEmpty == false{
            let font = UIFont.init(name: "Roboto-Light", size: 18.0) ?? UIFont.systemFont(ofSize: UIFont.systemFontSize)
            let arrtibutes = [NSForegroundColorAttributeName:placeholderColor!, NSFontAttributeName: font] as [String : Any]
            self.attributedPlaceholder = NSAttributedString(string: ph, attributes: arrtibutes)
        }
    }
    
    override open func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        contentVerticalAlignment = .center
        borderWidth = 1.0
        borderColor = UIColor.secondaryText
        placeholderColor = UIColor.secondaryText
        verticalAlignment = UIControlContentVerticalAlignment.bottom.rawValue
        self.borderStyle = .none
    }
    
    var bottomBorder:CALayer?
    
    override open var placeholder: String?{
        didSet{
            guard let ph = placeholder, ph.isEmpty == false else {return}
            let arrtibutes = [NSForegroundColorAttributeName:placeholderColor!]
            self.attributedPlaceholder = NSAttributedString(string: ph, attributes: arrtibutes)
        }
    }
    
    override open var intrinsicContentSize: CGSize{
        get{
            return CGSize(width: UIViewNoIntrinsicMetric, height: 40)
        }
    }
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x:bounds.origin.x, y:bounds.origin.y+5,width: bounds.size.width-30, height: bounds.size.height-5);
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return self.textRect(forBounds: bounds);
    }
    
    func addBottomBorder(){
        self.clipsToBounds = true
        bottomBorder?.removeFromSuperlayer()
        
        bottomBorder = CALayer()
        bottomBorder?.borderColor = borderColor?.cgColor
        bottomBorder?.borderWidth = borderWidth
        bottomBorder?.frame = CGRect(x: 0, y: self.frame.size.height-borderWidth, width: self.frame.size.width, height: borderWidth)
        self.layer.addSublayer(bottomBorder!)
    }
    
    override open func draw(_ rect: CGRect) {
    }
}
