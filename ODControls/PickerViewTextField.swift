//
//  PickerViewTextField.swift
//  Sipoc
//
//  Created by Farooq Zaman on 21/03/2017.
//  Copyright © 2017 Pizza Mania. All rights reserved.
//

import UIKit

public typealias CompletionHandler = (_ completed: Bool) -> (Void)

public extension UITextField{
    public func hidePickerView(completion:CompletionHandler? = nil){
        completion?(true)
    }
}

@IBDesignable
open class PickerViewTextField: BorderLessTextField {
    
    @IBOutlet public weak var pickerViewParent:UIStackView?
    @IBOutlet public weak var pvDelegate:UIPickerViewDelegate?
    @IBOutlet public weak var pvDataSource:UIPickerViewDataSource?
    
    @IBInspectable public var pickerViewIndex:Int = -1
    @IBInspectable public var cornerRadius:CGFloat = 5.0{
        didSet{
            textPickerView?.layer.cornerRadius = 5.0
        }
    }    
    @IBInspectable public var pickerViewHeight:CGFloat = 120{
        didSet{
            textPickerView?.pickerViewHeight = pickerViewHeight
        }
    }
    
    var textPickerView:TextPickerView?
    
    public var isPickerViewHidden:Bool{
        if textPickerView == nil{
            return true
        }
        return false
    }
    
    public func setDefaultOption(row:Int, component:Int){
        textPickerView?.pickerView.selectRow(row, inComponent: component, animated: true)
    }

    public func showPickerView(completion:CompletionHandler? = nil){
        textPickerView = createPickerView()
        textPickerView?.isHidden = true
        pickerViewParent?.insertArrangedSubview(textPickerView!, at: pickerViewIndex)
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: { 
            self.textPickerView?.isHidden = false
        }) { (finished) in
            completion?(true)
        }
    }
    
    override public func hidePickerView(completion:CompletionHandler? = nil){
        if let pvParent = textPickerView, pvParent.superview != nil{
            UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: { 
                pvParent.isHidden = true
            }, completion: { (completed) in
                self.pickerViewParent?.removeArrangedSubview(pvParent)
                pvParent.removeFromSuperview()
                self.textPickerView = nil
                completion?(true)
            })
        }else{
            completion?(true)
        }
    }
    
    func createPickerView()->TextPickerView?{
        let textPickerView:TextPickerView = .fromNib()
        textPickerView.layer.cornerRadius = cornerRadius
        textPickerView.pickerView.delegate = pvDelegate
        textPickerView.pickerView.dataSource = pvDataSource
        textPickerView.pickerViewHeight = pickerViewHeight
        return textPickerView
    }
}
