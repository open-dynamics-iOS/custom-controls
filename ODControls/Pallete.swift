//
//  Pallete.swift
//  HRDF Events
//
//  Created by Farooq Zaman on 21/11/2016.
//  Copyright © 2016 scouffe Pvt Ltd. All rights reserved.
//

import UIKit

extension UIColor {
    static var eggplant:            UIColor { get { return UIColor(rgba:"#180d32") } }
    static var bgdark:              UIColor { get { return UIColor(rgba:"#261b3e") } }
    static var primaryText:         UIColor { get { return UIColor(rgba:"#ffffff") } }
    static var secondaryText:       UIColor { get { return UIColor(rgba:"#887ead") } }
    static var accent:              UIColor { get { return UIColor(rgba:"#0097a7") } }
    static var accent2:             UIColor { get { return UIColor(rgba:"#2ED6B7") } }
    static var paging:              UIColor { get { return UIColor(rgba:"#9688ba") } }
    static var bgProgress:          UIColor { get { return UIColor(rgba:"#6343b2") } }
    static var bgCell:              UIColor { get { return UIColor(rgba:"#2F204F") } }
    
    convenience init(rgba: String) {
        var red:   CGFloat = 0.0
        var green: CGFloat = 0.0
        var blue:  CGFloat = 0.0
        var alpha: CGFloat = 1.0
        
        if rgba.hasPrefix("#") {
            let index   = rgba.characters.index(rgba.startIndex, offsetBy: 1)
            let hex     = rgba.substring(from: index)
            let scanner = Scanner(string: hex)
            var hexValue: CUnsignedLongLong = 0
            if scanner.scanHexInt64(&hexValue) {
                switch (hex.characters.count) {
                case 3:
                    red   = CGFloat((hexValue & 0xF00) >> 8)       / 15.0
                    green = CGFloat((hexValue & 0x0F0) >> 4)       / 15.0
                    blue  = CGFloat(hexValue & 0x00F)              / 15.0
                case 4:
                    red   = CGFloat((hexValue & 0xF000) >> 12)     / 15.0
                    green = CGFloat((hexValue & 0x0F00) >> 8)      / 15.0
                    blue  = CGFloat((hexValue & 0x00F0) >> 4)      / 15.0
                    alpha = CGFloat(hexValue & 0x000F)             / 15.0
                case 6:
                    red   = CGFloat((hexValue & 0xFF0000) >> 16)   / 255.0
                    green = CGFloat((hexValue & 0x00FF00) >> 8)    / 255.0
                    blue  = CGFloat(hexValue & 0x0000FF)           / 255.0
                case 8:
                    red   = CGFloat((hexValue & 0xFF000000) >> 24) / 255.0
                    green = CGFloat((hexValue & 0x00FF0000) >> 16) / 255.0
                    blue  = CGFloat((hexValue & 0x0000FF00) >> 8)  / 255.0
                    alpha = CGFloat(hexValue & 0x000000FF)         / 255.0
                default:
                    print("Invalid RGB string, number of characters after '#' should be either 3, 4, 6 or 8", terminator: "")
                }
            } else {
                print("Scan hex error")
            }
        } else {
            print("Invalid RGB string, missing '#' as prefix", terminator: "")
        }
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
}
