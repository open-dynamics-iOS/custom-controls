//
//  FetchedResultsTableViewController.swift
//  NearSnappIOS
//
//  Created by Farooq Zaman on 26/03/2017.
//  Copyright © 2017 MatrixCentric. All rights reserved.
//

import UIKit
import Realm
import RealmSwift
import SwiftFetchedResultsController
import SafeRealmObject
import DZNEmptyDataSet

open class FetchedResultsTableViewController: UITableViewController,FetchedResultsControllerDelegate {
    
    var emptyTitle:String?
    var emptyDescription:String?
    var emptyErrorButton:String?
    var emptyImage = UIImage(named:"empty_asset")

    override open func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
    }

    public func update(cell:UITableViewCell, at indexPath:IndexPath?){
        //Override in child class
    }
    
    public func tableIsEmpty(){
        //Override in child class
    }
    
    public func refreshContents(){
        //Override in child class
    }
    
    public func controllerWillChangeContent<T: Object>(_ controller: FetchedResultsController<T>) {
        tableView.beginUpdates()
    }
    
    public func controller<T: Object>(_ controller: FetchedResultsController<T>, didChangeObject anObject: SafeObject<T>, atIndexPath indexPath: IndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath! as IndexPath],
                                 with: .automatic)
        case .delete:
            tableView.deleteRows(at: [indexPath! as IndexPath],
                                 with: .automatic)
        case .update:
            if let cell = tableView.cellForRow(at: indexPath! as IndexPath){
                update(cell: cell, at: indexPath)
            }
        case .move:
            tableView.deleteRows(at: [indexPath! as IndexPath],
                                 with: .automatic)
            tableView.insertRows(at: [newIndexPath! as IndexPath],
                                 with: .automatic)
        }
    }
    
    public func controllerDidChangeContent<T: Object>(_ controller: FetchedResultsController<T>) {
        tableView.endUpdates()
        
        if controller.numberOfSections() == 0{
            tableIsEmpty()
        }else{
            hideEmptyMessage()
        }
    }
    
    public func controllerDidChangeSection<T:Object>(_ controller: FetchedResultsController<T>, section: FetchResultsSectionInfo<T>, sectionIndex: UInt, changeType: NSFetchedResultsChangeType) {
        let indexSet = IndexSet(integer: IndexSet.Element(sectionIndex))
        
        switch changeType {
        case .insert:
            tableView.insertSections(indexSet,
                                     with: .automatic)
        case .delete:
            tableView.deleteSections(indexSet,
                                     with: .automatic)
        default :
            break
        }
    }
}

//MARK: Empty Data Set
extension FetchedResultsTableViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate{
    public func showEmptyMessage(title:String?, desc:String? = nil, button:String? = nil){
        emptyTitle = title
        emptyDescription = desc
        emptyErrorButton = button
        emptyImage = UIImage(named:"empty_asset")
        self.tableView.reloadEmptyDataSet()
    }
    
    public func hideEmptyMessage(){
        emptyTitle = ""
        emptyDescription = ""
        emptyErrorButton = ""
        emptyImage = nil
        self.tableView.reloadEmptyDataSet()
    }
    
    public func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString!{
        if let emptyTitle = emptyTitle{
            let attr = [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 16.0), NSForegroundColorAttributeName:UIColor.primaryText]
            return NSAttributedString(string: emptyTitle, attributes: attr)
        }
        return NSAttributedString(string: "")
    }
    
    public func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString!{
        if let desc = emptyDescription{
            let attr = [NSFontAttributeName: UIFont.systemFont(ofSize: 14.0), NSForegroundColorAttributeName:UIColor.primaryText]
            return NSAttributedString(string: desc, attributes: attr)
        }
        return NSAttributedString(string: "")
    }
    
    public func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage!{
        return emptyImage
    }
    
    public func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> NSAttributedString!{
        if let buttonTitle = emptyErrorButton{
            let attr = [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 14.0), NSForegroundColorAttributeName:UIColor.accent]
            return NSAttributedString(string: buttonTitle.uppercased(), attributes: attr)
        }
        return NSAttributedString(string: "")
    }
    
    public func emptyDataSet(_ scrollView: UIScrollView!, didTap button: UIButton!){
        refreshContents()
    }
}
