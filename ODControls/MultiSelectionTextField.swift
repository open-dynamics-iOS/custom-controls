//
//  MultiSelectionTextField.swift
//  Sipoc
//
//  Created by Farooq Zaman on 06/04/2017.
//  Copyright © 2017 Pizza Mania. All rights reserved.
//

import UIKit

open class MultiSelectionTextField: BorderLessTextField {
    
    @IBOutlet public weak var viewParent:UIStackView?
    @IBOutlet public weak var optionsDelegate:UITableViewDelegate?
    @IBOutlet public weak var dataSource:UITableViewDataSource?
    
    @IBInspectable public var insertionIndex:Int = -1
    @IBInspectable public var cornerRadius:CGFloat = 5.0{
        didSet{
            multiSelectionView?.layer.cornerRadius = 5.0
        }
    }
    @IBInspectable public var viewHeight:CGFloat = 120{
        didSet{
            multiSelectionView?.viewHeight = viewHeight
        }
    }
    
    @IBInspectable public var cellIndentifier:String = "CellIdentifier"{
        didSet{
            let xib = UINib(nibName: cellIndentifier, bundle: Bundle.main)
            multiSelectionView?.tableView.register(xib, forCellReuseIdentifier: cellIndentifier)
        }
    }
    
    public var multiSelectionView:MultiSelectionView?
    var contentInset:UIEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0){
        didSet{
            multiSelectionView?.tableView.contentInset = contentInset
        }
    }
    
    public var isPickerViewHidden:Bool{
        if multiSelectionView == nil{
            return true
        }
        return false
    }
    
    public func setSelectedOptions(indexPaths:[IndexPath]){
        guard let tableView = multiSelectionView?.tableView else {return}
        
        for indexPath in indexPaths{
            tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
            tableView.delegate?.tableView!(tableView, didSelectRowAt: indexPath)
        }
    }
    
    public func showPickerView(completion:CompletionHandler? = nil){
        multiSelectionView = createMultiSelectionView()
        multiSelectionView?.isHidden = true
        viewParent?.insertArrangedSubview(multiSelectionView!, at: insertionIndex)
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: {
            self.multiSelectionView?.isHidden = false
        }) { (finished) in
            completion?(true)
        }
    }
    
    override public func hidePickerView(completion:CompletionHandler? = nil){
        if let pvParent = multiSelectionView, pvParent.superview != nil{
            UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: {
                pvParent.isHidden = true
            }, completion: { (completed) in
                self.viewParent?.removeArrangedSubview(pvParent)
                pvParent.removeFromSuperview()
                self.multiSelectionView = nil
                completion?(true)
            })
        }else{
            completion?(true)
        }
    }
    
    func createMultiSelectionView()->MultiSelectionView?{
        let multiSelectionView:MultiSelectionView = .fromNib()
        multiSelectionView.layer.cornerRadius = cornerRadius
        multiSelectionView.tableView.delegate = optionsDelegate
        multiSelectionView.tableView.dataSource = dataSource
        multiSelectionView.tableView.sizeToFit()
        multiSelectionView.tableView.allowsMultipleSelection = true
        multiSelectionView.viewHeight = viewHeight
        
        let xib = UINib(nibName: cellIndentifier, bundle: Bundle.main)
        multiSelectionView.tableView.register(xib, forCellReuseIdentifier: cellIndentifier)
        
        return multiSelectionView
    }
}
