//
//  PickerViewTextField.swift
//  Sipoc
//
//  Created by Farooq Zaman on 21/03/2017.
//  Copyright © 2017 Pizza Mania. All rights reserved.
//

import UIKit

@objc
public protocol DatePickerTextFieldDelegate:class{
    @objc optional func minimumDate(for textField:DatePickerTextField)->Date?
    @objc optional func maximumDate(for textField:DatePickerTextField)->Date?
    @objc optional func date(for textField:DatePickerTextField)->Date?
    @objc optional func mode(for textField:DatePickerTextField)->UIDatePickerMode
    func datePickerView(_ textField:DatePickerTextField, didSelectDate date:Date)
}

@IBDesignable
open class  DatePickerTextField: BorderLessTextField {
    
    @IBInspectable
    public var optionColor:UIColor = UIColor.white

    @IBOutlet public weak var pickerViewParent:UIStackView?
    @IBOutlet public weak var datePickerDelegate:DatePickerTextFieldDelegate?{
        didSet{
            datePickerView?.datePicker.minimumDate = datePickerDelegate?.minimumDate?(for: self)
            datePickerView?.datePicker.maximumDate = datePickerDelegate?.maximumDate?(for: self)
            
            if let date = datePickerDelegate?.date?(for: self){
                datePickerView?.datePicker.date = date
            }
            
            if let mode = datePickerDelegate?.mode?(for: self){
                datePickerView?.datePicker.datePickerMode = mode
            }
        }
    }
    
    @IBInspectable public var pickerViewIndex:Int = -1
    @IBInspectable public var cornerRadius:CGFloat = 5.0{
        didSet{
            datePickerView?.layer.cornerRadius = 5.0
        }
    }    
    @IBInspectable public var pickerViewHeight:CGFloat = 120{
        didSet{
            datePickerView?.pickerViewHeight = pickerViewHeight
        }
    }

    var datePickerView:DatePickerView?
    
    
    public var isPickerViewHidden:Bool{
        if datePickerView == nil{
            return true
        }
        return false
    }
    
    public func showPickerView(completion:CompletionHandler? = nil){
        datePickerView = createPickerView()
        datePickerView?.isHidden = true
        pickerViewParent?.insertArrangedSubview(datePickerView!, at: pickerViewIndex)
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: { 
            self.datePickerView?.isHidden = false
        }) { (finished) in
            completion?(true)
        }
    }
    
    override public func hidePickerView(completion:CompletionHandler? = nil){
        if let pvParent = datePickerView, pvParent.superview != nil{
            UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: { 
                pvParent.isHidden = true
            }, completion: { (completed) in
                self.pickerViewParent?.removeArrangedSubview(pvParent)
                pvParent.removeFromSuperview()
                self.datePickerView = nil
                completion?(true)
            })
        }else{
            completion?(true)
        }
    }
    
    func createPickerView()->DatePickerView?{
        let datePickerContainer:DatePickerView = .fromNib()
        datePickerContainer.layer.cornerRadius = cornerRadius
        datePickerContainer.pickerViewHeight = pickerViewHeight
        datePickerContainer.datePicker.addTarget(self, action: #selector(didSelectDate(sender:)), for: .valueChanged)
        datePickerContainer.datePicker.setValue(optionColor, forKeyPath: "textColor")
        datePickerContainer.datePicker.setValue(false, forKey: "highlightsToday")
        return datePickerContainer
    }
    
    func didSelectDate(sender:UIDatePicker){
        datePickerDelegate?.datePickerView(self, didSelectDate: sender.date)
    }
}
