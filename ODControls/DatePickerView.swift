//
//  TextPickerView.swift
//  Sipoc
//
//  Created by Farooq Zaman on 21/03/2017.
//  Copyright © 2017 Pizza Mania. All rights reserved.
//

import UIKit

class ChildDatePicker:UIDatePicker{
    var height:CGFloat = 120
    
    override var intrinsicContentSize: CGSize{
        get{
            return CGSize(width: UIViewNoIntrinsicMetric, height: height)
        }
    }
}

class DatePickerView: UIVisualEffectView {
    @IBOutlet var datePicker:ChildDatePicker!
    
    var pickerViewHeight:CGFloat = 120{
        didSet{
            datePicker.height = pickerViewHeight
            datePicker.invalidateIntrinsicContentSize()
        }
    }
}
