//
//  UIView+XIB.swift
//  Sipoc
//
//  Created by Farooq Zaman on 21/03/2017.
//  Copyright © 2017 Pizza Mania. All rights reserved.
//

import UIKit

public extension UIView {
    public class func fromNib<T : UIView>() -> T {
        return Bundle(for:T.self).loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}
