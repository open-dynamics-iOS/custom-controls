//
//  TextPickerView.swift
//  Sipoc
//
//  Created by Farooq Zaman on 21/03/2017.
//  Copyright © 2017 Pizza Mania. All rights reserved.
//

import UIKit

class ChildPickerView:UIPickerView{
    var height:CGFloat = 120
    
    override var intrinsicContentSize: CGSize{
        get{
            return CGSize(width: UIViewNoIntrinsicMetric, height: height)
        }
    }
}

class TextPickerView: UIVisualEffectView {
    @IBOutlet var pickerView:ChildPickerView!
    
    var pickerViewHeight:CGFloat = 120{
        didSet{
            pickerView.height = pickerViewHeight
            pickerView.invalidateIntrinsicContentSize()
        }
    }
}
