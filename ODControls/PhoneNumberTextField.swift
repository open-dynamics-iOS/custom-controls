//
//  PhoneNumberTextField.swift
//  Sipoc
//
//  Created by Farooq Zaman on 22/03/2017.
//  Copyright © 2017 Pizza Mania. All rights reserved.
//

import UIKit
import libPhoneNumber_iOS

@IBDesignable
open class PhoneNumberTextField: BorderLessTextField {

    fileprivate var phoneFormatter: NBAsYouTypeFormatter = NBAsYouTypeFormatter(regionCode: Locale.current.regionCode)
    
    var _delegate:UITextFieldDelegate?
    override open var delegate: UITextFieldDelegate?{
        didSet{
            if !(delegate is PhoneNumberTextField){
                _delegate = delegate
                self.delegate = self
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override open func didMoveToSuperview() {
        super.didMoveToSuperview()
    }
    
    @IBInspectable
    public var regionCode:String? = Locale.current.regionCode{
        didSet{
            phoneFormatter = NBAsYouTypeFormatter(regionCode: regionCode)
            self.text = formattedPhoneNumber(self.text, region: regionCode ?? "MY")
        }
    }
    
    func formattedPhoneNumber(_ phoneNumber:String?, region:String)->String{
        if let phone = phoneNumber , phone.characters.count > 0 && phone != "N/A"{
            let phoneUtil = NBPhoneNumberUtil()
            
            let prettyNumber = phone.trimmingCharacters(in: CharacterSet.decimalDigits.inverted)
            
            do{
                let number:NBPhoneNumber = try phoneUtil.parse(prettyNumber, defaultRegion:region)
                let formattedNumber = try NBPhoneNumberUtil().format(number, numberFormat: NBEPhoneNumberFormat.NATIONAL)
                return formattedNumber
            }catch _{
                return phoneNumber!
            }
        }
        return ""
    }
}

extension PhoneNumberTextField:UITextFieldDelegate{
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool{
        return _delegate?.textFieldShouldBeginEditing?(textField) ?? true
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField){
        _delegate?.textFieldDidEndEditing?(textField)
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool{
        return _delegate?.textFieldShouldEndEditing?(textField) ?? true
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField){
        _delegate?.textFieldDidEndEditing?(textField)
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason){
        _delegate?.textFieldDidEndEditing?(textField, reason: reason)
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        if range.length == 1 {
            textField.text = phoneFormatter.removeLastDigit()
        } else {
            textField.text = phoneFormatter.inputDigit(string)
        }
        
        NotificationCenter.default.post(name: NSNotification.Name.UITextFieldTextDidChange, object: textField)
        return false
    }
    
    public func textFieldShouldClear(_ textField: UITextField) -> Bool{
        phoneFormatter.clear()
        return _delegate?.textFieldShouldClear?(textField) ?? true
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        return _delegate?.textFieldShouldReturn?(textField) ?? true
    }
}
