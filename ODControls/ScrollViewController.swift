//
//  ScrollViewController.swift
//  NearSnappIOS
//
//  Created by Farooq Zaman on 25/03/2017.
//  Copyright © 2017 MatrixCentric. All rights reserved.
//

import UIKit

open class ScrollViewController: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet public weak var scrollview: UIScrollView!
    open var activeView:UIView?
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        let gesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        gesture.delegate = self
        self.view.addGestureRecognizer(gesture)
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let nc = NotificationCenter.default
        nc.addObserver(self, selector: #selector(keyboardDidShow), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        nc.addObserver(self, selector:#selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override open func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let nc = NotificationCenter.default
        
        nc.removeObserver(self, name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        nc.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    public func keyboardDidShow(_ notification: Notification){
        if let info = notification.userInfo as? [String:AnyObject]{
            if var kbRect = info[UIKeyboardFrameBeginUserInfoKey]?.cgRectValue{
                kbRect = self.view.convert(kbRect, from:nil)
                let contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbRect.size.height, 0.0)
                self.scrollview.contentInset = contentInsets
                self.scrollview.scrollIndicatorInsets = contentInsets
                
                var aRect = self.view.frame
                aRect.size.height -= kbRect.size.height
                
                if let activeView = self.activeView{
                    var tvRect = activeView.convert(activeView.bounds, to: self.view)
                    let point = CGPoint(x:tvRect.origin.x, y: tvRect.origin.y + tvRect.size.height)
                    if(!aRect.contains(point)){
                        tvRect.size.height = tvRect.size.height
                        self.scrollview.scrollRectToVisible(tvRect, animated: true)
                    }
                }
            }
        }
    }
    
    public func keyboardWillHide(_ notification:Notification){
        let contentInsets = UIEdgeInsets.zero
        self.scrollview.contentInset = contentInsets
        self.scrollview.scrollIndicatorInsets = contentInsets
    }
    
    dynamic open func dismissKeyboard(gesture:UITapGestureRecognizer?){
        activeView?.resignFirstResponder()
    }
    
    dynamic open func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool{
        return true
    }
}
