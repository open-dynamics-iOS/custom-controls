//
//  MultiSelectionView.swift
//  Sipoc
//
//  Created by Farooq Zaman on 06/04/2017.
//  Copyright © 2017 Pizza Mania. All rights reserved.
//

import UIKit

open class ChildTableView:UITableView{
    var height:CGFloat = 200
    
    override open var intrinsicContentSize: CGSize{
        get{
            return CGSize(width: UIViewNoIntrinsicMetric, height: height)
        }
    }
}

open class MultiSelectionView: UIVisualEffectView {
    @IBOutlet public var tableView:ChildTableView!
    
    var viewHeight:CGFloat = 200{
        didSet{
            tableView.height = viewHeight
            tableView.invalidateIntrinsicContentSize()
        }
    }
}
